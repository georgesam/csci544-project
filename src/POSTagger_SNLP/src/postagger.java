import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONObject;

import edu.stanford.nlp.international.arabic.process.ArabicDocumentReaderAndWriter;
import edu.stanford.nlp.io.FileUtils;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.util.StringUtils;


public class postagger {
	
	public static ArrayList<String> listOfTags=new ArrayList<String>();
	
	
	public static ArrayList<String> tagList = new ArrayList<String>();
	
	public static void userPreProcessor(String directoryPath) throws IOException{
		
		
		File folder = new File(directoryPath);
		FileWriter tempOutWriter = new FileWriter("preProcessedUsers_20.txt");
		File[] listOfFiles = folder.listFiles();
		int fileCount =0;
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		    	String[] tempTok = StringUtils.splitOnChar(file.getName().toString(),'.');
		    	String fileName = tempTok[0];
		    
					BufferedReader br = new BufferedReader(new FileReader(file));
					String line;
					while((line = br.readLine()) != null){
						
						tempOutWriter.write(fileName+" "+line+"\n");
						System.out.println(fileName+" "+line);
						
					}
		    }
		    
		    fileCount++;
		    if(fileCount == 20) return;
		}
		        
		    
		
	}
	public static void main(String[] args) throws IOException{
		
		
		String filename="preProcessedUsers_20.txt";
		
		/*String sample = "This is some new sample text to be pos tagged.";
		
		String tagged_text = tagger.tagString(sample);	
		
		System.out.println(tagged_text);*/
		//userPreProcessor("/home/gsam/BitBucket/Project/twitter_data/data/datasets-tokenized/training-set-tokenized/");
		//POSgenerator(filename);
		generatePOSCount("tweetPOSTagged_20.txt");
	}
	
	public static void POSgenerator(String filename) throws IOException{
		MaxentTagger tagger = new MaxentTagger("taggers/gate-EN-twitter.model");
		FileWriter fw = new FileWriter("tweetPOSTagged_20.txt");
		int tweetCount =0;
		
		for(String line: FileUtils.linesFromFile(filename)){
	
	            //line = br.readLine();
	            
	            String tweetLine = line.toString();
	            
	            if(tweetLine.equals("")){
	            	System.out.println("NULL");
	            	continue;
	            }
	            //System.out.println("LINE: "+tweetLine);
		        //System.out.println(tweetLine);
		        //DocumentPreprocessor dp =new DocumentPreprocessor(filename);
		        
		       /* PTBTokenizer tokenizer = new PTBTokenizer(new FileReader(filename), new CoreLabelTokenFactory(), "");
		        
		        for (CoreLabel label; tokenizer.hasNext();){
		        	label = (CoreLabel) tokenizer.next();
		        	System.out.println(label);
		        }*/
		        
		        List<String> tokens = StringUtils.split(tweetLine);
		        System.out.println(tweetCount);
		        
		        
		        /*for(String tok:tokens){
		        	System.out.println(tokens);
		        }*/
		        	
		        String userName = tokens.get(0);
		        ArrayList<String> tokenList = new ArrayList<String>();
		        String tokenizedString ="";
		        int tokenCount =0;
		       for (Iterator<String> iter = tokens.listIterator(); iter.hasNext(); ) {
		        		    String a = iter.next();
		        		    //System.out.println(userName);
		        		    if (a.equals(userName)){
		        		        continue;
		        		    }
		        		    else
		        		    {
		        		    	tokenizedString+=a+" ";
		        		    }
		        		    
		        		    tokenCount++;
		        		    //System.out.println(tokenCount);
		        		    
		        		}
		       //System.out.println("LINE: "+ tokenizedString + "END");
		       
		       String taggedTweet = tagger.tagTokenizedString(tokenizedString);
		       
		       //System.out.println(taggedTweet);
		       fw.write(userName+" "+taggedTweet+"\n");
		       tweetCount++;
		       
		       
	        
		}
		
		fw.close();
		System.out.println("POS Tagging Complete");
	        
	        
	}	
	
	public static void generatePOSCount(String posTaggedFile) throws IOException{
		HashMap<String, HashMap> uNameHash = new HashMap<String, HashMap>(); 
		FileWriter tempFW = new FileWriter("tempfile.txt");
		String[] lisOfTags = {"USR", "PRP", "VBD", "CC", "IN", "JJS", "NN", "NNS", "DT", "VBP", "VB", "VBG", "JJ", "UH", "RB", "TO", "VBN", "PRP$", "NNP", "VBZ", "URL", "WP", "MD", "WRB", "RT", "SYM", "CD", "WDT", "RP", "EX", "JJR", "RBR", "HT", "RBS", "MB", "POS", "PDT", "WP$", "FW", "$", "NNPS", "ON", "R", "LV", "FM", "PR", "J", "HR", "AE", "F", "O", "N", "CCG", "EA", "MP", "P", "T"};
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(posTaggedFile));
			String line;
			while((line = br.readLine()) != null){
				//System.out.println(line+"\n-----");
				List<String> tokens = StringUtils.split(line);
				String userName = tokens.get(0);
				//System.out.println(userName);
				if(!uNameHash.containsKey(userName)){
					//System.out.println("NOT there");
					//System.exit(0);
					HashMap<String, Integer> posHash = new HashMap<String, Integer>();
					
					
					uNameHash.put(userName, posHash);
					for(String tagVal: lisOfTags){
						posHash.put(tagVal, 0);
					}
					
					
				}
				Iterator<String> iter = tokens.listIterator();
				iter.next();
				while( iter.hasNext() ) {
					
					//System.exit(0);
					String words = iter.next();
					String[] posTokens=StringUtils.splitOnChar(words, '_');
					
					String wordTok = posTokens[0];
					String posTag = posTokens[1];
					System.out.println(wordTok+" ::: "+posTag);
					tempFW.write(wordTok+" ::: "+posTag+"\n");
					HashMap posHash = uNameHash.get(userName);
					
					
				
					
					if((posTag.matches("[A-Z $]+"))){
						
					
					if(!posHash.containsKey(posTag)){
					
						
						/*if(!tagList.contains(posTag)){
							tagList.add(posTag.toString());
						}*/
						//posHash.put(posTag, 1);
						continue;
					}
					else{
						int tagCount = (int) posHash.get(posTag);
						tagCount++;
						posHash.put(posTag, tagCount);
					}
					
					}
					
					
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject hashJSON = new JSONObject();
		hashJSON.putAll(uNameHash);
		FileWriter outFileWriter = new FileWriter("hashMapCount.txt");
		//outFileWriter.write(uNameHash);
		ObjectOutputStream oStream = new ObjectOutputStream(new FileOutputStream("hashMapCount.txt"));
		//oStream.writeObject(hashJSON.toJSONString());
		//oStream.flush();
		System.out.println(uNameHash.toString());
		System.out.println("JSON Written");
		
		FileWriter tagWriter = new FileWriter("tagList.txt");
		tagWriter.write(tagList.toString());
		tagWriter.close();
		//tempFW.close();
		System.out.println("Tag list written");
	}
	

}
