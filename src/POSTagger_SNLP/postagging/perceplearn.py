import json
import sys
import operator
import random

def learnFromTraining(trainingFileName, modelFileName):
	lines = open(trainingFileName).readlines()
	random.shuffle(lines)
	open(trainingFileName, 'w').writelines(lines)
	featureVector = {}
	currFeatureVector = {}
	classDict={}
	wordCount = 0
	docCount =0

	with open(trainingFileName, 'r', encoding = 'utf-8', errors='ignore') as fin:
		for line in fin:
			#print (line)

			content = line.split()
			content = list(filter(None, content))

			if(len(content) == 0):
				continue

			wordNo = 0
			docCount+=1
			currFeatureVector[docCount] = {}
			currFeatureVector[docCount]['features'] = {}

			for word in content:
				wordNo+=1

				if wordNo == 1:

					currFeatureVector[docCount]['class'] = word
					if word not in classDict:
						classDict[word]=1
				else:
					if word not in featureVector:
						featureVector[word] = wordCount
						wordCount+=1

					if word not in currFeatureVector[docCount]['features']:
						currFeatureVector[docCount]['features'][featureVector[word]] = 1
					else:
						currFeatureVector[docCount]['features'][featureVector[word]] += 1
	
	weights={}
	avgWeights={}

	for className in classDict:
		weights[className]=[0]*wordCount
		avgWeights[className]=[0]*wordCount			

	for  i in range(10):
		print("iteration: "+str(i))

		for line in sorted(currFeatureVector.keys()):
			maxLabel=float('-inf')
			#print(str(currFeatureVector[line]))
			
			currClass = currFeatureVector[line]['class']
			
			for className in weights:

				label = 0
				for feature in currFeatureVector[line]['features']:
					label= label + currFeatureVector[line]['features'][feature] * weights[className][feature]

				if label > maxLabel:
					maxLabel = label
					maxClass = className

			if maxClass != currClass:
				for feature in currFeatureVector[line]['features']:
					weights[maxClass][feature] -= currFeatureVector[line]['features'][feature]
					weights[currClass][feature] += currFeatureVector[line]['features'][feature]

		#print("calc avg")
		if i!=0:
			for class1 in weights:
				avgWeights[class1] = [x + y for x, y in zip(weights[class1], avgWeights[class1])]



	modelFileDict = {}
	modelFileDict['features'] = featureVector
	modelFileDict['weights'] = avgWeights


	with open(modelFileName, 'w', encoding='utf-8') as fout:
		print(json.dumps(modelFileDict), file=fout)

if __name__=='__main__':
	trainingFileName = sys.argv[1]
	modelFileName = sys.argv[2]
	lines = open(trainingFileName).readlines()
	random.shuffle(lines)
	open(trainingFileName, 'w').writelines(lines)
	learnFromTraining(trainingFileName, modelFileName)