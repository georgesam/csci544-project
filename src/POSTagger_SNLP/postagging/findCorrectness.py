import re
import math
import os
import sys


def findCorrectness(lines):
    totalCount = 0
    correctCount=0
    mismatchCount = 0
    i = 0
    lines[:] = [line.rstrip('\n') for line in lines] 
    lines = list(filter(None, lines))
    print("xxx"+lines[1])
    exit()
    while i<len(lines):
        originalLine = lines[i].strip("\n")
        resultLine = lines[i+1].strip("\n")
        originalLine = re.sub("\n+","",originalLine)
        resultLine = re.sub("\n+","",resultLine)
        originalLine = originalLine.split(" ")
        resultLine = resultLine.split(" ")
        j=0

        while j < len(originalLine):
            originalWords = originalLine[j].split("/")
            if len(originalWords) == 2:
                originalWord = originalWords[1]
                resultWords = resultLine[j].split("/")
                resultWord = resultWords[1]
                if(originalWord != resultWord):
                    mismatchCount +=1
                if(originalWord == resultWord):
                    correctCount+=1
                totalCount +=1    
            j+=1
        i +=2
    print("total : " + str(totalCount))
    print("correct : "+str(correctCount))
    print("mismatch : " + str(mismatchCount))
    print("Accuracy : "+str((totalCount-mismatchCount)/totalCount))
    print("Error : " + str(mismatchCount/totalCount))


if __name__ == "__main__":
    inputPath = sys.argv[1]
    f = open(inputPath,"r")
    lines = f.readlines()
    #print(lines)
    f.close()
    findCorrectness(lines)