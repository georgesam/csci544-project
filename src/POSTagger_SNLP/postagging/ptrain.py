import sys
import os



sys.path.insert(0, "..")
from perceplearn import learnFromTraining


import re
trainingFileName = sys.argv[1]
modelFileName = sys.argv[2]
tempFile = "tempFile.txt"

def get_wshape_word(word):
	
	word = re.sub('[a-z]', 'a', word)
	word = re.sub('a+', 'a', word)
	word = re.sub('[A-Z]', 'A', word)
	word = re.sub('A+', 'A', word)
	word = re.sub('[0-9]', '0', word)
	word = re.sub('9+', '9', word)
	
	return word


def getFeatures(content, i):

	wordToken = content[i].split('/')
	currWord = wordToken[0]
	#print(currWord)
	currTag = wordToken[-1]

	suff3 = currWord[-3:]
	suff2= currWord[-2:]

	#print(currWord)
	wordShape = get_wshape_word(currWord)
	#print(i)
	
	if(len(content) ==1):
		prevWord = "START"
		#nextWordToken = content[i+1].split('/')
		nextWord = "END"
	else:
		if i==0:
			#print(content)
			prevWord = "START"
			nextWordToken = content[i+1].split('/')
			nextWord = nextWordToken[0]

		elif i== (len(content)-1):
			

			prevWordToken = content[i-1].split('/')
			prevWord = prevWordToken[0]
			nextWord = "END"
			
			
		else:
			prevWordToken = content[i-1].split('/')
			prevWord = prevWordToken[0]
			nextWordToken = content[i+1].split('/')
			nextWord = nextWordToken[0]

	#print(currTag+" "+"prev:" + prevWord+ " curr:" +currWord+ " next:" + nextWord+ " suff3:"+ suff3+ " suff2:"+ suff2 + " wordShape:"+ wordShape+"\n")
	return(currTag+" "+"prev:" + prevWord+ " curr:" +currWord+ " next:" + nextWord+ " suff3:"+ suff3+ " suff2:"+ suff2 + " wordShape:"+ wordShape+"\n")
	
outputString=""
with open(trainingFileName, 'r') as fin:
	for line in fin:
		content = line.split()
		count =0
		contentLen= len(content)
		#print(str(contentLen))
		for i in range(0, contentLen-1):
			
			tempString = getFeatures(content, i)
			outputString+= tempString

#print(outputString)
with open(tempFile, 'w', encoding='utf-8') as fout:
	print(outputString, file=fout)

#print("preceplearn called")

learnFromTraining(tempFile, modelFileName)