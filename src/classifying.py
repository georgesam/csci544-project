'''
Created on Apr 19, 2015

@author: reihane
'''
import sys
import os
import nltk
def extractFeatures(tweet):
    features = {}
    if '@' in tweet:
        features['mention']='yes'
    else:
        features['mention']='no'
    return features
def createFeaturesetNB(dirname):
    featureset = []
    for fname in os.listdir(dirname):
        tweets = open(dirname+'/'+fname)
        username = fname.split('.txt')[0]
        temp = [(extractFeatures(t), username) for t in tweets]
        featureset += temp
    return featureset
# def classifyNB():
#     train_set = createFeaturesetNB(sys.argv[1])
#     dev_set = createFeaturesetNB(sys.argv[2])
#     classifier = nltk.NaiveBayesClassifier.train(train_set)
# #     print(nltk.classify.accuracy(classifier, dev_set))
#     obj =  (classifier.prob_classify(extractFeatures('@kevincorrigan and the academy award for TV goes to ... Me ! :-)')))
#     print obj.prob('AlexanderWatt')

# classify()
from sklearn import svm
from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB

import numpy as np
from scipy import sparse
from scipy.sparse import *
from scipy import *
import random

#user number 11: CarrieWilkerson
# user number 21: ems
# user number 51: KenJennings
def NBfeatures(filename):
    tweets = open(filename).readlines()
    featuresname = tweets[0].split('\n')[0].split(' ')[1:] #feature names are separated by space
    tweets = tweets[1:]
    tweetscnt = len(tweets)
    featureset = []
    
    for i in range(0,tweetscnt):
        tempuser = tweets[i].split('\n')[0].split(',')[0]
        if(tempuser=='CarrieWilkerson'): # The name of the user you want to stop there. (here is the first 20 users). We assume that users are ordered alphabetically
            break
        tempfeatures = tweets[i].split('\n')[0].split(',')[1:] # feature values are separated by comma
        tempset = [(dict(zip(featuresname, tempfeatures)), tempuser)]
        featureset+=tempset
    return featureset

def NBbow(filename):
    tweets = open(filename).readlines()
    tweetscnt = len(tweets)
    featureset = []
    useriddict = {}
    id=-1
    for i in range(0,tweetscnt):
        tempuser = tweets[i].split('\n')[0].split()[0]
        if tempuser not in useriddict:
            id+=1
            useriddict[tempuser]=id
        if(tempuser=='CarrieWilkerson'): # The name of the user you want to stop there. (here is the first 20 users). We assume that users are ordered alphabetically
            break 
        tempfeatures = tweets[i].split('\n')[0].split()[1:] # feature values are separated by comma
        tempset = [(dict(zip(tempfeatures, tempfeatures)), useriddict[tempuser])]
        featureset+=tempset
    return featureset


def classifyNB(trainfile, devfile,bow, totalscore):
    if bow==1:
        trainset = NBbow(trainfile)
        devset = NBbow(devfile)
    else:
        trainset = NBfeatures(trainfile)
        devset = NBfeatures(devfile)


def classifyNB():
#     trainset = NBfeatures(sys.argv[1])
#     devset = NBfeatures(sys.argv[2])
    trainset = NBbow(sys.argv[1])
    devset = NBbow(sys.argv[2])
    random.shuffle(trainset)
    classifier = nltk.NaiveBayesClassifier.train(trainset)
    if totalscore==1:
        print(nltk.classify.accuracy(classifier,devset))
    else:
        devset_list = [x[0] for x in devset]
        return classifier.classify_many(devset_list)
#     print classifier.show_most_informative_features(10)
      
def SVMfeatures(filename, doc2vec):    
    tweets = open(filename).readlines()
    tweets = tweets[1:]
    tweetscnt = len(tweets)
    featureset = []
    usernames = []
    useriddict = {}
    id=-1
    for i in range(0,tweetscnt):
        tempuser = tweets[i].split(',')[0]
        #if tempuser=='CarrieWilkerson': # The name of the user you want to stop there. (here is the first 20 users). We assume that users are ordered alphabetically
           #break
        
        if doc2vec==1:
            featureset.append(map(float, tweets[i].split(',')[1:]))
        else:
            featureset.append(map(float, tweets[i].split('\n')[0].split(',')[1:])) # feature values are separated by comma
        
        if tempuser in useriddict:
            usernames.append(useriddict[tempuser])
        else:
            id+=1
            print(id)
            useriddict[tempuser]=id
            usernames.append(useriddict[tempuser])
    return featureset, usernames       

import sklearn.preprocessing
def classifySVM(trainfile, devfile, doc2vec, totalscore):
    normal = sklearn.preprocessing.StandardScaler()
    trainset_features, trainset_users = SVMfeatures(trainfile, doc2vec)
    devset_features, devset_users = SVMfeatures(devfile, doc2vec)
    trainset_features = normal.fit_transform(trainset_features)
    devset_features = normal.fit_transform(devset_features)
    clf = svm.LinearSVC(class_weight='auto')
    clf.fit(trainset_features, trainset_users)
    if totalscore==1:
        print clf.score(devset_features, devset_users)
    else:
        return clf.predict(devset_features), devset_users

def convertdata(filename):
    tweets = open(filename).readlines()
    featuresname = tweets[0].split('\n')[0].split(',') #feature names should be separated by space
    tweets = tweets[1:]
    tweetscnt = len(tweets)
    featureset = []
    for i in range(0,tweetscnt):
        tempfeatures = tweets[i].split('\n')[0].split(',') # feature values should be separated by comma
        tempset = [(dict(zip(featuresname, tempfeatures))) ]
        featureset+=tempset
    v = DictVectorizer(sparse = True)
    X = v.fit_transform(featureset)
    lenrow =  X.shape[0]
    lencol =  X.shape[1]
    for k in range(lenrow):
        tmpar = X[k].toarray()
        for m in range(lencol): 
            print tmpar[0][m]

# classifySVM(sys.argv[1], sys.argv[2],0, 1)
# classifyNB(sys.argv[1], sys.argv[2],1)

def combineClassifiers():
    mainclusterres,actualusers  = classifySVM(sys.argv[1], sys.argv[2],0, 0)
    doc2vecres,actualusers = classifySVM(sys.argv[3], sys.argv[4],1, 0)
    bowres = classifyNB(sys.argv[5], sys.argv[6], 1, 0)
    print type (mainclusterres) , type (doc2vecres), type (bowres)
    print len(mainclusterres) , len(doc2vecres), len(bowres)
    labels = []
    for i in range (len(actualusers)):
        if (mainclusterres[i] == bowres[i]):
            labels.append(bowres[i])
        else:
            labels.append(doc2vecres[i])
    acc = 0.0
    for i in range(len(actualusers)):
        if actualusers[i]==labels[i]:
            acc+=1.0
    acc = acc/len(actualusers)        
    print acc
    
#combineClassifiers()
def separateTweetUser():
    usertweets = open(sys.argv[1]).readlines()
    tweetfile = open('tweets','w')
    userfile = open('users','w')
    for i in usertweets:
        tweetfile.write(str(i.split('\t')[1]))
        userfile.write(str(i.split('\t')[0])+'\n')

# separateTweetUser()

def newSVMfeatures(filename):    
    tweets = open(filename).readlines()
    tweetscnt = len(tweets)
    featureset = []
    usernames = []
    useriddict = {}
    id=-1
    for i in range(0,tweetscnt):
        tempuser = tweets[i].split('\n')[0].split('\t')[0]
        #if tempuser=='CarrieWilkerson': # The name of the user you want to stop there. (here is the first 20 users). We assume that users are ordered alphabetically
            #break
        tempfeatures = tweets[i].split('\n')[0].split('\t')[1]
        featureset.append(tempfeatures)
        if tempuser in useriddict:
            usernames.append(useriddict[tempuser])
        else:
            id+=1
            useriddict[tempuser]=id
            usernames.append(useriddict[tempuser])
    return featureset, usernames

def newSVM():
    tweetfile = open('tfidf_train','w')
    tweetfile1 = open('tfidf_dev','w')

    tfidf = Pipeline([
    ('vectorizer', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ])
    normal = sklearn.preprocessing.StandardScaler()

    
    trainset_features, trainset_users = newSVMfeatures(sys.argv[1])
    devset_features, devset_users = newSVMfeatures(sys.argv[2])
    
    tempTrain = tfidf.fit_transform(trainset_features)
    tempDev = tfidf.transform(devset_features)
    
    tempTrain = sklearn.preprocessing.normalize(tempTrain)
    tempDev = sklearn.preprocessing.normalize(tempDev)

    print(tempTrain.shape)
    
    trainset_features1, trainset_users1 = SVMfeatures(sys.argv[3], 1)
    devset_features1, devset_users1 = SVMfeatures(sys.argv[4], 1)

    trainset_features1 = np.array(trainset_features1)
    devset_features1 = np.array(devset_features1)

    trainset_features1 = sklearn.preprocessing.normalize(trainset_features1)
    devset_features1 = sklearn.preprocessing.normalize(devset_features1)

    print(trainset_features1.shape)

    trainset_features2, trainset_users2 = SVMfeatures(sys.argv[5], 0)
    devset_features2, devset_users2 = SVMfeatures(sys.argv[6], 0)

    trainset_features2 = np.array(trainset_features2)
    devset_features2 = np.array(devset_features2)

    trainset_features2 = sklearn.preprocessing.normalize(trainset_features2)
    devset_features2 = sklearn.preprocessing.normalize(devset_features2)

    final_train_features = sparse.hstack((tempTrain, trainset_features1))
    final_dev_features = sparse.hstack((tempDev, devset_features1))

    final_train_features1 = sparse.hstack((final_train_features, trainset_features2))
    final_dev_features1 = sparse.hstack((final_dev_features, devset_features2))

    print(final_train_features1.shape)
    print(final_dev_features1.shape)
    
    text_clf = SGDClassifier().fit(final_train_features1, trainset_users)
    predicted = text_clf.predict(final_dev_features1)
    print np.mean(predicted == devset_users)
    

#classifySVM()
#classifyNB()
newSVM()