# CSCI544 - Applied Natural Language Processing
# Spring 2015
# Final Project - Author Identification on Twitter
# Extracting Twitter word clusters features
# Nada Aldarrab		naldarra@usc.edu


import sys
import random
import codecs

def extractFeatures(ipfile, opfile, mfile):
	# Character encoding
	encoding = 'utf-8'
	with codecs.open(ipfile, 'r', encoding) as ipfile:
		with codecs.open(opfile, 'w', encoding) as opfile:
			with codecs.open(mfile, 'r', encoding) as mfile:
				clusters = dict()
				for line in mfile:
					wtype = line.split('\t')[1]
					cluster = line.split('\t')[0]
					clusters.update({wtype:cluster})

				cluster_ind = dict()
				ind = 0
				for cluster in clusters.values():
					if cluster not in cluster_ind and cluster:
						cluster_ind.update({cluster:ind})
						ind += 1

				cluster_list = sorted(cluster_ind.keys())
				unk_count = 0
				for line in ipfile:
					tweet = line.split('\t', 1)[1]
					tokens = tweet.lower().split()
					if tokens:
						features = ''
						for token in tokens:
							if token in clusters:
								features += clusters[token] + ','
							elif token[0:4]=='http':
								features += 'U' + ','
							else:
								features += token + ','
								unk_count += 1
						
						features = features.replace(',,',',')
						opfile.write(features[:-1])	
						opfile.write('\n')

				print(unk_count)


if __name__ == '__main__':
	# Check if proper arguments have been provided 
	if len(sys.argv) > 3:
		# Get input file path
		ipfile = sys.argv[1] 
		# Get output file path
		opfile = sys.argv[2] 
		# Get model file path
		mfile = sys.argv[3] 
		extractFeatures(ipfile, opfile, mfile)
	else:
		print ('Please specify the path for the input, output and model files.')


