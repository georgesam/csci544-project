'''
Created on Jan 31, 2015

@author: georg_000
'''
import sys
precisionbaseFile=''
outputFile=''
def fillScoreDictionaries():
    baseFileList=[]
    baseClassCountDict=dict()
    outputClassCountDict=dict()
    correctPrediction=dict()
    with open(precisionbaseFile, 'r', errors='ignore',encoding='utf-8 ') as baseFile:
        data=baseFile.readlines()
    count=0    
    for lineData in data:
        classData = lineData.split(' ')[0]
        classCount = count
        count+=1
        
        if(classData not in baseClassCountDict):
            baseClassCountDict[classData]=1
        else:
            baseClassCountDict[classData]+=1
        baseFileList.append(classData+' '+str(count))
        if classData not in correctPrediction.keys():
            correctPrediction[classData]=0    
    #print(baseClassCountDict)
   
    with open(outputFile, 'r', errors='ignore',encoding='utf-8 ') as baseFile:
       outputData=baseFile.readlines()
       
    tempcount=0
    for lines in outputData:
        classData = lines.split(' ')[0]
        classNum = tempcount
        tempcount+=1
        if(classData not in outputClassCountDict):
            outputClassCountDict[classData]=1
        else:
            outputClassCountDict[classData]+=1
            
        if(classData+' '+str(tempcount) in baseFileList):
            correctPrediction[classData]+=1
            
    #print(outputClassCountDict)
    #print(correctPrediction)
    fScoreSum=0
    for classNames in baseClassCountDict.keys():
        precision = float(correctPrediction[classNames] / outputClassCountDict[classNames])
        recall = float(correctPrediction[classNames] / baseClassCountDict[classNames])
        
        fScore = (2 * precision* recall / (precision + recall))
        print ('Class : '+classNames)
        print ('Precision : %f'%precision)
        print('Recall : %f'%recall)
        print('F-Score : %f'%fScore)
        fScoreSum+=fScore
    fScoreSum=fScoreSum/(len(baseClassCountDict.keys()))
    print("\n ::: Avg FSCORE : "+str(fScoreSum))
    
    
def main():
    global precisionbaseFile
    global outputFile
    precisionbaseFile=sys.argv[1]
    outputFile=sys.argv[2]
    fillScoreDictionaries() 
if __name__ == "__main__": main()
 
