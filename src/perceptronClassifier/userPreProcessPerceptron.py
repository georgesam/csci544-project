'''
Created on Apr 26, 2015

@author: gsam

vowpal wabbit preprocessing
'''
import csv
import os
import string
from os import listdir
import sys
import json
import pickle

#csv_f = csv.reader(f)
columnList={}
fHash = {}
userNameList={}



uList=[]

filename = sys.argv[1]
outfile = sys.argv[2]

def readUserTraining():
  count =0
  fieldCount =0
  classCount =1
  global columnList
  global userNameList
  global uList
  tCount =0
  with open(filename) as myfile:
      

      for line in myfile:
        
            if count == 0:
                fields=line.split(' ')
                #print(fields)
                for columnNames in fields:
                    columnNames=columnNames.strip('\n')
                    if(columnNames=='|'):
                      columnNames='PIPE'
                    if(columnNames==':'):
                      columnNames='COLON'
                    columnList[fieldCount]=str(columnNames)
                    fieldCount+=1
                    
                print(columnList)
                #sys.exit()
      
              
            else:
                uName = line.split(',')[0]
                #
                if(uName not in userNameList.keys()):
                    userNameList[uName]=classCount
                    uList.append(uName)
                    classCount+=1
                    fHash[uName]={}
                    tweetNo=0
                    print(uName)
                
                uHash = fHash.get(uName) 
                
                uHash[tweetNo]={}
                
                #featureVect = str(userNameList[uName])+" | " 
                fCount=0
                line=line.strip("\n")   
                featureValList = line.split(',')
                #print(featureValList)
                tweetHash = uHash.get(tweetNo)
                tweetNo+=1
                #print(type(tweetHash))
                for vals in featureValList:
                    
                    if(fCount == 0):
                        fCount+=1 
                        continue
                    #print(columnList[fCount]+":"+str(vals)+" ")
                    
                    #featureVect+= columnList[fCount]+":"+str(vals)+" "
                    feat = columnList[fCount]
                    

                    tweetHash[feat]=vals
                    #print(columnList[fCount]+": "+str(vals))
                    fCount+=1
                    #print(str(fCount))
                #print(featureVect)
                #outputFilePtr.write(featureVect)
                #    break
                featureVect=''
               
                #featureVect.flush()
              
            count+=1
  pickle.dump(fHash, open("features.p","wb"))
  pickle.dump(uList, open("users.p","wb"))
  pickle.dump(userNameList, open("userID.p","wb"))
  with open('featureHash.json', 'w', encoding='utf-8') as fout:
      print(json.dumps(fHash), file=fout)
  print("\n ** done reading ** \n")
#outputFilePtr.close()

def createFeatureSpace():

  #print(str(tCount))
  outputFilePtr = open(outfile, 'w')
  uList= pickle.load(open("users.p","rb"))
  userNameList= pickle.load(open("userID.p","rb"))
  fHash = pickle.load(open("features.p","rb"))
  k=0
  #print(userNameList)

  userSet =[]
  for user in uList:
    #user = uList[i]
    
    userSet.append(user)
    if(k == uCount):
      break
    k+=1
    classNum = userNameList[user]
    hash1 = fHash.get(user)

    
    for t in range(0,len(hash1.keys())):

      #print(hash1.get(t))
      #print("\n\nXXXXX "+ str(t)+" XXXX\n\n")
      print(user)
      userSet.append(user)
      hash2 = hash1.get(t)
      featureVect=''
      featureVect = user+" "
      fc=0 
      if(t==0 and k==1):
        print(type(sorted(hash2.keys())))
        
      '''if type(hash2) == "NoneType":
        print(type(sorted(hash2.keys())))
        print(str(hash3))'''  

      
      for keyVal in sorted(hash2.keys()):
        featureVect+= keyVal+":"+hash2.get(keyVal).strip("\n")+" "
        
        fc+=1
      hash3 = hash2
        

      
      outputFilePtr.write(featureVect+"\n")
    #print(user)
  userKey=open('userSetDev.txt','w')  
  for u in userSet:
    userKey.write(str(u)+"\n")
  userKey.close()
  outputFilePtr.close

  print("\n** vector space created **\n")

  




uCount = 20
tCount = 1000


readUserTraining()
createFeatureSpace()