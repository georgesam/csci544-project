# CSCI544 - Applied Natural Language Processing
# Spring 2015
# Final Project - Author Identification on Twitter
# Tokenize tweets (TweetNLP)
# Nada Aldarrab		naldarra@usc.edu


import glob
import sys
import codecs
import os


def tokenize(ipfolder, opfolder):
	# Character encoding
	encoding = 'utf-8'
	# Create path name to data files
	tdpath = ipfolder + '*.txt'
	# Make a list of all available data documents
	tdset = glob.glob(tdpath)
	# Processing each document
	for document in tdset:
		# Get the filename
		filename = document.split('/')[-1]	
		with codecs.open(document, 'r', encoding) as ipfile:
			with codecs.open((opfolder+filename), 'w', encoding) as opfile:
				os.system('./twokenize.sh ' + document + ' > ' + (opfolder+filename))

		
if __name__ == '__main__':
	# Check if proper arguments have been provided 
	if len(sys.argv) > 2:
		# Get input folder path
		ipfolder = sys.argv[1] 
		# Get output folder path
		opfolder = sys.argv[2]
		tokenize(ipfolder, opfolder)
	else:
		print ('Please specify the path of the input and output folders as (e.g. users/).')
		
